package Komla2.TestFxml;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class CalculatorController {
	String operand1 = "";
	String operand2 = "";
	String operator;
	

	public void initialize() {
		System.out.println("calculator booted! ");
	}
	@FXML Label result;
	
	@FXML
	
private void inputDigit(Event event) {
		Button btn = (Button) event.getTarget();
		String digit = btn.getText();
	System.out.println("Digit Click: ");
	if(operator == null) {
		operand1 += digit;
		result.setText(operand1);
	}
	else {
		operand2 += digit;
		result.setText(operand1 +operator + operand2);
	}
	System.out.println("opereand1: " + operand1 + " opereand2: " +operand2);
}
	@FXML
	// tracer les button
private void inputSign(Event event) {
		System.out.println("Sign Click: ");
		Button btn = (Button) event.getTarget();
		String sign = btn.getText();
		if(hasOperand1() && hasOperand2()) {
			return;
		}
		if(sign.equals("-")) {
			if(hasOperand1()) {
				operand1 += "-";
			}
			else if(!hasOperand2()) {
				operand2 += "-";
			}
			return;
		}
		if (operand1.length() > 0) {
			operator = sign;
		}
		
}
	@FXML
	// tracer les button
private void inputEquals(Event event) {
		System.out.println("Equals Click: ");
		if(hasOperand1() && hasOperand2()) {
			double n1 = Double.valueOf(operand1);
			double n2 = Double.valueOf(operand2);
			Double r = null; 
			switch(operator) {
			case "+": r = n1 + n2;break;
			case "-": r = n1 - n2;break;
			case "X": r = n1 * n2;break;
			case "/": r = n1 / n2;break;
			
			}
			result.setText(String.valueOf(r));
		}
		
}
	
	private boolean hasOperand1() {
		return operand1.length() > 0;
	}

	private boolean hasOperand2() {
		return operand2.length() > 0;
	}
	@FXML
	// tracer les button
private void inputDoit() {
		if(operator == null) {
			operand1 += ".";
		}
		else {
			operand2 += ".";
		}
		System.out.println("Dote Click: ");
		
}
}
