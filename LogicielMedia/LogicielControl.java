package Komla2.logicielMedia;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;



public class LogicielControl {
	@FXML
	ArrayList<Album> album = new ArrayList<Album>();
	@FXML
	ArrayList<Artiste> art = new ArrayList<Artiste>();
	@FXML
	ArrayList<Album> film = new ArrayList<Album>();
	@FXML
	ArrayList<Artiste> acteur= new ArrayList<Artiste>();
	@FXML
	ScrollPane scrol;
	@FXML
	Label serch;
	@FXML
	Pane musicPane;
	@FXML
	Pane moviePane;
	@FXML
	Label afficheInfo;
	@FXML
	Button mubtn;
	@FXML
	Button filbtn;
	@FXML
	Button btn;
	@FXML
	Button btn2;
	
	@FXML
	private TextField serchbar;
	@FXML
	VBox listeMusic;
	@FXML
	VBox listeFilms;
	@FXML
	VBox nameArtiste;
//	@FXML
//	VBox nameActeur;
	ObservableList<TextField> data;
boolean btnPressed;
	public void initialize() {
		
		System.out.println("MediaView! ");
		ContextMenu menus = new ContextMenu();
		//liste d'artiste
		
		
		System.out.println(art);
		// les musique ajout de l'album
		album.add(new Album("Ho", "dit", "Dancehall"));
		album.add(new Album("Hu", "diot", "PoP"));
		album.add(new Album("do", "dit", "Reagger"));
		album.add(new Album("Mo", "diot", "Raï"));
		album.add(new Album("Ho", "dit", "Funk"));
		album.add(new Album("Hu", "diot", "Rock"));
		album.add(new Album("do", "dit", "Soul"));
		album.add(new Album("Mo", "diot", "Zouk"));
		album.add(new Album("Ho", "dit", "R'n&B"));
		album.add(new Album("Hu", "diot", "Rap"));
		album.add(new Album("do", "dit", "Salsa"));
		album.add(new Album("Mo", "diot", "Raï"));
		album.add(new Album("Ho", "dit", "Jazz"));
		album.add(new Album("Hu", "diot", "Country"));
		album.add(new Album("do", "dit", "Disco"));
		album.add(new Album("Mo", "diot", "Blues"));
		album.add(new Album("Mo", "diot", "Flamenco"));
		
//les films
		film.add(new Album("dimo", "dit", "Comedie"));
		film.add(new Album("solo", "dit", "Documentaire"));
		film.add(new Album("dimo", "dit", "Action"));
		film.add(new Album("solo", "dit", "Aventure"));
		film.add(new Album("dimo", "dit", "Espionnage"));
		film.add(new Album("solo", "dit", "Fantastique"));
		film.add(new Album("dimo", "dit", "Horreur"));
		film.add(new Album("solo", "dit", "Science-fiction"));
		film.add(new Album("dimo", "dit", "Policier"));
		film.add(new Album("solo", "dit", "Drame"));
		film.add(new Album("dimo", "dit", "Guerre"));
		film.add(new Album("solo", "dit", "Suspense"));
		film.add(new Album("dimo", "dit", "Opéra"));
		film.add(new Album("solo", "dit", "Thriller"));
		film.add(new Album("dimo", "dit", "Epee"));
		film.add(new Album("solo", "dit", "Western"));
		
		// le nom des artistes
		
		art.add(new Artiste("Sean Paul"));
		art.add(new Artiste("50 cent"));
		art.add(new Artiste("BOB Marley"));
		art.add(new Artiste("Elephan Man"));
		art.add(new Artiste("Brandy"));
		art.add(new Artiste("Kelly"));
		art.add(new Artiste("Daddy Yankee"));
		art.add(new Artiste("Bronie"));
		art.add(new Artiste("Shabba"));
		art.add(new Artiste("Shaggy"));
		art.add(new Artiste("Brandy"));
		art.add(new Artiste("Eminem"));
		// les acteur
		acteur.add(new Artiste("Bruce Lee"));
		
		ArrayList<String> genre = gerDeFilm();
		ArrayList<String> categorie = categoDemusic();
		ArrayList<String> artiste= nomArtiste();
		ArrayList<String> acteurs = mesActeurs();
		for (String s : categorie) {
			btn = new Categorie(s);
			btn.setText(s);
			listeMusic.getChildren().add(btn);
			btn.setPrefWidth(140);
			btn.setPrefHeight(60);
			
			
		}
		
		for (String g : genre) {
			 btn = new Categorie(g);
			btn.setText(g);
			listeFilms.getChildren().add(btn);
			btn.setPrefWidth(140);
			btn.setPrefHeight(60);
		
			
		}
		for (String a : artiste) {
			 btn = new Categorie(a);
			btn.setText(a);
			nameArtiste.getChildren().add(btn);
			btn.setPrefWidth(140);
			btn.setPrefHeight(60);
			
			
		}
		for (String a : acteurs) {
			 btn = new Categorie(a);
			btn.setText(a);
		nameArtiste.getChildren().add(btn);
			btn.setPrefWidth(140);
			btn.setPrefHeight(60);
			
			
		}
		
	}

	private ArrayList<String> categoDemusic() {
		ArrayList<String> resltCageto = new ArrayList<String>();
		
		for (Album a : album) {
			String categorie = a.getCategorie();
			System.out.println(categorie);
			if (!resltCageto.contains(categorie)) {
				resltCageto.add(categorie);
				
			}

		}
		return resltCageto;
	}

	private ArrayList<String> gerDeFilm() {
		ArrayList<String> genreFim = new ArrayList<String>();
		for (Album f : film) {
			String genre = f.getCategorie();
			System.out.println(genre);
			if (!genreFim.contains(genre)) {
				genreFim.add(genre);
			}

		}
		return genreFim;
	}

	private ArrayList<String>nomArtiste(){
		ArrayList<String> nameArt= new ArrayList<String>();
		for(Artiste a: art) {
			String name = a.getName();
			System.out.println(name);
			if(!nameArt.contains(name)) {
				nameArt.add(name);
			}
		}
		return nameArt;
	}
	
	private ArrayList<String>mesActeurs(){
		ArrayList<String> acteurs= new ArrayList<String>();
		for(Artiste a: acteur) {
			String name = a.getName();
			System.out.println(name);
			if(!acteurs.contains(name)) {
				acteurs.add(name);
			}
		}
		return acteurs;
	}
	
	public	ArrayList<Artiste> findArtistes(String name){
		ArrayList<Album> result = new ArrayList<Album>();
		for(Album a: album) {
			for(Artiste art:art) {
				if( art.getName().equals(name)) {
					result.add(a);
					System.out.println(art);
				}
			}
			
		}
		return art;
	}
	
	@FXML
	private void inputAuteur1(String categorie ) {
		
		// serchbar.setText(album);
		if(gerDeFilm().equals(categorie) ) {
			
		}
scrol.setVisible(true);
		System.out.println("Play music !");

	}

	@FXML
	private void serchBar() {
		// serchbar.getContextMenu();
		System.out.println();
	}

	@FXML
	void mediaomepageMusic(ActionEvent event) {
		mubtn = (Button) event.getTarget();
		data = FXCollections.observableArrayList(new TextField(""));
		int compteur;
		ObservableList<TextField> input = FXCollections.observableArrayList();

		String[] info = { "nom", "prenom" };
		data.add(new TextField("magieffo"));
		data.add(new TextField("Georg"));
		for (int i = 0; i < data.size(); i++) {
			System.out.println(data.get(i).getText());
			serch.setText(data.get(i).getText());
		}
		if (event.getSource().equals(mubtn)) {
			serchbar.setText("hello :");
			input.add(serchbar);

			serch.setText(String.valueOf(mubtn.getText()));

		}
		System.out.println(data.size() + " " + data.get(2).getText() + " " + mubtn.getText());

		serch.setText(serchbar.getText() + mubtn.getText());

	}

	@FXML
	void mediaomepage(ActionEvent event) {
		filbtn = (Button) event.getTarget();
		data = FXCollections.observableArrayList(new TextField(""));
		int compteur;
		ObservableList<TextField> input = FXCollections.observableArrayList();
		input.toArray();

		String[] info = { "nom", "prenom" };
		data.add(new TextField("magieffo je suis le next"));
		data.add(new TextField("Georg i m gost"));
		for (int i = 0; i < data.size(); i++) {
			System.out.println(data.get(i).getText());
			serch.setText(data.get(i).getText());
		}
		if (event.getSource().equals(filbtn)) {
			serchbar.getText();
			input.add(serchbar);
			serch.setText(String.valueOf(filbtn.getText()));

		}
		System.out.println(data.size() + " " + data.get(2).getText() + " " + filbtn.getText());

		serch.setText(serchbar.getText() + filbtn.getText());

	}
	public class Categorie extends Button implements  EventHandler<ActionEvent> {
		 String categorie;
		
//		 Button btn = new Button(categorie);
		
	public Categorie(String categorie) {
		super(categorie);
		this.categorie = categorie;
		setOnAction(this);
		
	
		
	}
		
		public void handle(ActionEvent event) {
//			
		
			System.out.println(categorie);
			 setText(categorie);
			serch.setText(categorie.toUpperCase());
			serchbar.setText(categorie);
			nomArtiste().toString();
			nameArtiste.getChildren().clear();
		
			for(Album a: album) {
			
				if(a.getCategorie().equals(categorie)) {
					for(Artiste e : art) {
						btn = new Button(e.getName());
						nameArtiste.getChildren().add(btn);
						
						btn.setPrefWidth(140);
						btn.setPrefHeight(60);
						
					}
						
				}
				
					
			}
			for(Album a: film) {
				
				if(a.getCategorie().equals(categorie)) {
					for(Artiste e : acteur) {
						btn = new Button(e.getName());
						nameArtiste.getChildren().add(btn);
						
						btn.setPrefWidth(140);
						btn.setPrefHeight(60);
						
					}
						
				}
				
					
			}
			scrol.setVisible(true);
		}

	}

	public class Media extends Categorie{
private HashMap<Artiste, Album > media ;
		public Media(String categorie) {
			super(categorie); 
		media = new HashMap<Artiste, Album >();
		}
		
	}
}

