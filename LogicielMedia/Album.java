package Komla2.logicielMedia;

import java.util.*;


public class Album {
	private String nomdeAlbum;
	private String date;
	private ArrayList< Album>album;
private ArrayList< Artiste>artiste;
private String categorie;
	public Album() {
	
	}
	public Album(String name, String nomdeAlbum,String categarie ) {
		this.nomdeAlbum = nomdeAlbum;
		this.getNomdeAlbum();
		this.getDate();
		
		this.categorie =categarie;
		artiste = new ArrayList< Artiste>();
		album = new  ArrayList< Album>();
	}
	
	/**
	 * @return the album
	 */
	public ArrayList<Album> getAlbum() {
		return album;
	}
	/**
	 * @param album the album to set
	 */
	public void setAlbum(ArrayList<Album> album) {
		this.album = album;
	}
	/**
	 * @return the artiste
	 */
	public ArrayList<Artiste> getArtiste() {
		return artiste;
	}
	/**
	 * @param artiste the artiste to set
	 */
	public void setArtiste(ArrayList<Artiste> artiste) {
		this.artiste = artiste;
	}
	public void ajouterUnAlbum(Album a) {
		album.add(a);
	}
	public void ajouterUnArtiste(Artiste a) {
		artiste.add(a);
	}
public	ArrayList<Artiste> findArtistes(String name){
		ArrayList<Album> result = new ArrayList<Album>();
		for(Album a: album) {
			for(Artiste art:artiste) {
				if( art.getName().equals(name)) {
					result.add(a);
				}
			}
			
		}
		return artiste;
		
	
	}
	public String toString(){
		String str = "la date de sorie:" + this.getDate() +this.getNomdeAlbum();
		str += "[";
		for(int i = 0; i <album.size();i++) {
			str += ""+ album.get(i);
		}
		str +="]";
	return str;
		
	}
	public String getNomdeAlbum() {
		return nomdeAlbum;
	}
	public void setNomdeAlbum(String nomdeAlbum) {
		this.nomdeAlbum = nomdeAlbum;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
}
