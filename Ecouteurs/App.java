package Komla2.ecouteur;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

  static int clicks= 0;
    @Override
    public void start(Stage stage) {
      BorderPane root = new BorderPane();
      Button btn = new Button("CLIQUE MOI !");
      root.setCenter(btn); 
      btn.setOnAction(new HandleClick()); // pose de l'écouteur
      Scene scene = new Scene(root, 400, 400);
      stage.setTitle("Objet Event");
      stage.setScene(scene);
      stage.show();
   
    }

    // Implémentation d'un écouteur de type ActionEvent
    class HandleClick implements EventHandler<ActionEvent> {
      @Override
      public void handle(ActionEvent event) {
        System.out.println(event.getEventType()); // affiche le type de l'event
        System.out.println(event.getTarget()); // affiche la source de l'event 
        Button btn =(Button)event.getTarget();
        App.clicks++;
        btn.setText(String.valueOf(App.clicks));
        
      }
    }
    public static void main(String[] args) {
        launch();
    }
    }

